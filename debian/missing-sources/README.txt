This directory provides the non-minified versions of the embedded JQuery files
in the orig source tarball, in order to comply with Debian Policy.

The minified javascript files for which this directory provides the
non-minified versions are as follows. Each of the lines below indicate the
minified file, followed by the non-minified source file(s) present in this
directory.

doc/docu/jquery.js: (JQuery 1.3.2)
    jquery.js
